DECLARE
	v_region_id regions.region_id%TYPE := 2 ;
BEGIN
	MERGE INTO region_copy c
	USING regions r
	ON(r.region_id = v_region_id)
	WHEN MATCHED THEN 
		UPDATE SET 
			c.region_id = r.region_id,
			c.region_name = r.region_name
	WHEN NOT MATCHED THEN 
		INSERT VALUES (r.region_id,r.region_name);
END ;
/