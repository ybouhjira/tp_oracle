--------------------------------------------------------------------------------
-- I . EXCEPTION PREDEFINIES 
--------------------------------------------------------------------------------
DECLARE
    v_my_number NUMBER ;
    v_string VARCHAR2(256) := 'text';
BEGIN
    v_my_number := TO_NUMBER(v_string);

EXCEPTION
    WHEN VALUE_ERROR THEN
        DBMS_OUTPUT.PUT_LINE('Impossible de convertir v_string en nombre');
END;
/

-- un autre exemple
DECLARE
    v_emp employees.last_name%TYPE ;
BEGIN
    SELECT last_name 
    INTO v_emp
    FROM employees
    WHERE salary < 100 ;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('No employee found');
END;
/

-- un exemple avec zero_divide
DECLARE
    v_divide_result NUMBER ;
BEGIN
    SELECT salary/NVL(commission_pct,0)
    INTO v_divide_result
    FROM employees
    WHERE employee_id = 100;

EXCEPTION
    WHEN ZERO_DIVIDE THEN
        DBMS_OUTPUT.PUT_LINE('division par zero');
END;
/

--------------------------------------------------------------------------------
-- II. EXCEPTION NON PREDEFINIES DU SERVEUR ORACLE
--------------------------------------------------------------------------------

SET SERVEROUTPUT ON
DECLARE
    e_reg EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_reg,-2292);
BEGIN
    DELETE FROM regions 
    WHERE region_id = 2 ;
    COMMIT;
EXCEPTION
    WHEN e_reg THEN
          DBMS_OUTPUT.PUT_LINE('countries still exist in this region');
END;
/

--------------------------------------------------------------------------------
-- II. FONCTION D'INTERCEPTION DES EXCEPTION
--------------------------------------------------------------------------------

 

--------------------------------------------------------------------------------
-- II. INTERCEPTER DES EXCEPTIONS DEFIFNIES PAR L'UTILISATEUR
--------------------------------------------------------------------------------

DECLARE
    v_emp_id employees.employee_id%TYPE := 55 ;
    e_invalid_emp EXCEPTION ;
BEGIN
    UPDATE employees
    SET salary = salary + 100
    WHERE employee_id = v_emp_id;

    IF SQL%NOTFOUND THEN
        RAISE e_invalid_emp ;
    END IF;

EXCEPTION
    WHEN e_invalid_emp THEN
        DBMS_OUTPUT.PUT_LINE('No such employee');
END;
/


-- Utiliser raise_application_error
CREATE FUNCTION get_employee_email( p_last_name IN VARCHAR2 ) IS
DECLARE
    v_email employees.email%TYPE ;
BEGIN
    SELECT UPPER(SUBSTR(FIRST_NAME,1,1)) || UPPER(REPLACE(LAST_NAME,' ','')) ;
    IN v_email
    WHERE last_name = p_last_name ;
    RETURN v_email ;
EXCEPTION 
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20200, 'No such employee')  ;
END;