SET SERVEROUTPUT ON
DECLARE
	v_lowest_min_salary jobs.min_salary%TYPE ;
	v_msg VARCHAR2(200) := 'Le salaire minimum le plus bas est :' ;
BEGIN
	SELECT MIN(min_salary)
	INTO v_lowest_min_salary
	FROM jobs;

	DBMS_OUTPUT.PUT_LINE(v_msg||TO_CHAR(v_lowest_min_salary));
END;
/