SET SERVEROUTPUT ON
DECLARE
	v_last_name employees.last_name%TYPE;
BEGIN
	SELECT last_name
	INTO v_last_name
	FROM employees
	WHERE salary = (
		SELECT MAX(salary)
		FROM employees
	);
	IF v_last_name = 'ahmed' THEN 
		DBMS_OUTPUT.PUT_LINE('ahmed');
	ELSE IF v_last_name = 'youssef' THEN
		DBMS_OUTPUT.PUT_LINE('youssef');
	ELSE 
		DBMS_OUTPUT.PUT_LINE('NO');
	END IF;
END;
/