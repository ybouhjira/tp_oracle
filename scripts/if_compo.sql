SET SERVEROUTPUT ON
DECLARE
	v_regionn regions.region_name%TYPE;
	v_countryn countries.country_name%TYPE;
	v_i NUMBER(1);
	v_j NUMBER(1);
	v_region_count NUMBER(1);
	v_count NUMBER (2);
BEGIN
	SELECT COUNT(region_id)
	INTO v_region_count
	FROM regions ;
	<<outer_loop>>
	 LOOP
		SELECT region_name
		INTO v_regionn 
		FROM regions
		WHERE   region_id= v_i;
		DBMS_OUTPUT.PUT_LINE(v_regionn);
		-- compter le nombre de pays 
		SELECT COUNT(country_id)
		INTO v_count
		FROM countries
		NATURAL JOIN regions;   
		<<inner_loop>>
		LOOP
		 	SELECT country_name
		 	INTO v_countryn
		 	FROM countries
		 	WHERE country_id = v_j AND region_id = v_i;
		 	DBMS_OUTPUT.PUT_LINE(v_countryn);
		 	v_j = v_j+1;
		 	EXIT inner_loop WHEN v_j = v_count-1; 	
	    END LOOP inner_loop;
	    v_i= v_i+1;
		EXIT outer_loop WHEN v_i = v_region_count-1;    
    END LOOP outer_loop;
END;
/